//
//  Scene.swift
//  PokemonAR
//
//  Created by Ignacio Hernaiz Izquierdo on 29/1/19.
//  Copyright © 2019 Ignacio Hernaiz Izquierdo. All rights reserved.
//

import SpriteKit
import ARKit
import GameplayKit

class Scene: SKScene {
    
    let remainingLabel = SKLabelNode()
    var timer: Timer?
    var targetsCreated = 0
    var targetCount = 0 {
        didSet {
            self.remainingLabel.text = "Faltan: \(targetCount)"
        }
    }
    
    let deathSound = SKAction.playSoundFileNamed("QuickDeath", waitForCompletion: false)
    
    let startTime = Date()
    
    override func didMove(to view: SKView) {
        // Setup your scene here
        //Configuracion del HUD
        remainingLabel.fontSize = 30
        remainingLabel.fontName = "Avenir Next"
        remainingLabel.color = .white
        remainingLabel.position = CGPoint(x: view.frame.midX-190, y: 150)
        addChild(remainingLabel)
        
        targetCount = 0
        
        //Creacion enemigos cada 3 segundos
        timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true, block: { (timer) in
            self.createTarget()
        })
        
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard let sceneView = self.view as? ARSKView else {
//            return
//        }
//        
//        // Create anchor using the camera's current position
//        if let currentFrame = sceneView.session.currentFrame {
//            
//            // Create a transform with a translation of 0.2 meters in front of the camera
//            var translation = matrix_identity_float4x4
//            /* Pos = (0,0,0), Rot = (0,0,0)
//              0 1 2 3
//             [1,0,0,0] -> x
//             [0,1,0,0] -> y
//             [0,0,1,0] -> z
//             [0,0,0,1] -> t
//             */
//            translation.columns.3.z = -0.2
//            /* Pos = (0,0,-0.2), Rot = (0,0,0)
//              0 1 2    3
//             [1,0,0,   0] -> x
//             [0,1,0,   0] -> y
//             [0,0,1,-0.2] -> z
//             [0,0,0,   1] -> t
//             */
//            let transform = simd_mul(currentFrame.camera.transform, translation)
//            
//            // Add a new anchor to the session
//            let anchor = ARAnchor(transform: transform)
//            sceneView.session.add(anchor: anchor)
//        }
        
        //localizar el primer toque del cojunto de toques
        //mirar si el toque cae dentro de nuestra vista de AR
        guard let touch = touches.first else {return}
        let location = touch.location(in: self)
        print("El toque ha sido en: (\(location.x), \(location.y))")
        
        //buscaremos todos los nodos que han sido tocados por ese toque de usuario
        let hit = nodes(at: location)
        
        //cogeremos el primer sprite del array que nos devuelve el método anterior (si lo hay)
        //y animaremos ese pokemon hasta hacerlo desaparecer
        if let sprite = hit.first {
            let scaleOut = SKAction.scale(to: 2, duration: 0.4)
            let fadeOut = SKAction.fadeOut(withDuration: 0.4)
            let remove = SKAction.removeFromParent()
            let groupAction = SKAction.group([scaleOut,fadeOut, deathSound])
            let sequenceAction = SKAction.sequence([groupAction, remove])
            
            sprite.run(sequenceAction)
            
            //actualizaremos que hay ub pokemon menos con la variable target count
            targetCount -= 1
            
            if targetsCreated == 25 && targetCount == 0 {
                gameOver()
            }
        }
        
        
    }
    
    func createTarget() {
        if targetsCreated == 25 {
            timer?.invalidate()
            timer = nil
            return
        }
        targetsCreated += 1
        targetCount += 1
        
        guard let sceneView = self.view as? ARSKView else {
            return
        }
        
        //Crear generador de numeros aleatorios
        let random = GKRandomSource.sharedRandom()
        
        //Crear matriz de rotacion aleatoria en X
        let rotateX = matrix_float4x4.init(SCNMatrix4MakeRotation(2.0 * Float.pi * random.nextUniform(), 1, 0, 0))
        
        //Crear matriz de rotacion aleatoria en Y
        let rotateY = matrix_float4x4.init(SCNMatrix4MakeRotation(2.0 * Float.pi * random.nextUniform(), 0, 1, 0))
        
        //Combinar las dos rotaciones con un producto de matrices
        let rotation = simd_mul(rotateX, rotateY)
        
        //Crear una traslacion de 1.5 metros en la direccion de la pantalla
        var translation = matrix_identity_float4x4
        translation.columns.3.z = -1.5
        
        //Combinar la rotacion y la translacion
        let finalTransform = simd_mul(rotation, translation)
        
        //Creamos un punto ancla en el punto final determinado
        let anchor = ARAnchor(transform: finalTransform)
        
        //Añadir el ancla a la escena
        sceneView.session.add(anchor: anchor)
        
    }
    
    func gameOver() {
        //Ocultar la remainiglabel
        remainingLabel.removeFromParent()
        
        //Crear una nueva imagen con la foto de gameover
        let gameOver = SKSpriteNode(imageNamed: "gameover")
        addChild(gameOver)
        
        //Calcular el tiempo que le ha llevado al usuario cazar a todos los pokemon
        let timeTaken = Date().timeIntervalSince(startTime)
        
        //Mostrar ese tiempo que le ha llevado en pantalla en una nueva etiqueta
        let timeTakenLabel = SKLabelNode(text: "Te ha llevado: \(Int(timeTaken))")
        timeTakenLabel.fontSize = 40
        timeTakenLabel.color = .white
        timeTakenLabel.position = CGPoint(x: view!.frame.maxX - 50,
                                          y: -view!.frame.midY + 50)
        addChild(timeTakenLabel)
    }
}
